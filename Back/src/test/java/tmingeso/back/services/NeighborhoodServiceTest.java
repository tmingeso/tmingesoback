package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.NeighborhoodModel;
import tmingeso.back.repositories.MunicipalityRepository;
import tmingeso.back.repositories.NeighborhoodRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class NeighborhoodServiceTest {

    @TestConfiguration
    static class NeighborhoodServiceTestContextConfiguration {
        @Bean
        public NeighborhoodService neighborhoodService() {
            return new NeighborhoodService();
        }
    }

    @Autowired
    private NeighborhoodService service;

    @MockBean
    private NeighborhoodRepository repository;

    @MockBean
    private MunicipalityRepository municipalityRepository;

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DIRECTION = "TEST_DIRECTION";
    private static final String PHONE = "TEST_PHONE";
    private static final String MAIL = "TEST_MAIL";

    @Before
    public void setUpGetAll() {
        NeighborhoodModel neighborhood1 = new NeighborhoodModel();
        NeighborhoodModel neighborhood2 = new NeighborhoodModel();
        NeighborhoodModel neighborhood3 = new NeighborhoodModel();
        List<NeighborhoodModel> neighborhoodList = new ArrayList<>();
        neighborhoodList.add(neighborhood1);
        neighborhoodList.add(neighborhood2);
        neighborhoodList.add(neighborhood3);
        Mockito.when(repository.findAll()).thenReturn(neighborhoodList);
    }

    @Before
    public void setUpFindById() {
        NeighborhoodModel neighborhood = new NeighborhoodModel();
        neighborhood.setId(ID);
        Optional<NeighborhoodModel> optionalNeighborhood = Optional.of(neighborhood);

        Mockito.when(repository.findById(neighborhood.getId()))
                .thenReturn(optionalNeighborhood);
    }

    @Before
    public void setUpCreate() {
        NeighborhoodModel neighborhood = new NeighborhoodModel();
        NeighborhoodModel found = new NeighborhoodModel();
        found.setNeighborhoodName(NAME);
        found.setNeighborhoodDirection(DIRECTION);
        found.setNeighborhoodPhone(PHONE);
        found.setNeighborhoodMail(MAIL);
        Mockito.when(repository.save(neighborhood)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<NeighborhoodModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        NeighborhoodModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("name","TEST_NAME");
        obj.put("direction","TEST_DIRECTION");
        obj.put("phone","TEST_PHONE");
        obj.put("mail","TEST_MAIL");
        obj.put("municipality",1);

        NeighborhoodModel found = service.create(obj);
        Assertions.assertThat(found.getNeighborhoodName()).isEqualTo(NAME);
    }

    @Test
    public void delete() {
        NeighborhoodModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }
}