package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.CategoryModel;
import tmingeso.back.repositories.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class CategoryServiceTest {

    @TestConfiguration
    static class CategoryServiceTestContextConfiguration {
        @Bean
        public CategoryService categoryService() {
            return new CategoryService();
        }
    }

    @Autowired
    private CategoryService service;

    @MockBean
    private CategoryRepository repository;

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";

    @Before
    public void setUpGetAll() {
        CategoryModel category1 = new CategoryModel();
        CategoryModel category2 = new CategoryModel();
        CategoryModel category3 = new CategoryModel();
        List<CategoryModel> categoryList = new ArrayList<>();
        categoryList.add(category1);
        categoryList.add(category2);
        categoryList.add(category3);
        Mockito.when(repository.findAll()).thenReturn(categoryList);
    }

    @Before
    public void setUpFindById() {
        CategoryModel category = new CategoryModel();
        category.setId(ID);
        Optional<CategoryModel> optionalCategoryModel = Optional.of(category);

        Mockito.when(repository.findById(category.getId()))
                .thenReturn(optionalCategoryModel);
    }

    @Before
    public void setUpCreate() {
        CategoryModel category = new CategoryModel();
        CategoryModel found = new CategoryModel();
        found.setName(NAME);
        Mockito.when(repository.save(category)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<CategoryModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        CategoryModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("name","TEST_NAME");

        CategoryModel found = service.create(obj);
        Assertions.assertThat(found.getName()).isEqualTo(NAME);
    }

    @Test
    public void delete() {
        CategoryModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }
}