package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.UserModel;
import tmingeso.back.repositories.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {
        @Bean
        public UserService userService() {
            return new UserService();
        }
    }

    @Autowired
    private UserService service;

    @MockBean
    private UserRepository repository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private NeighborhoodRepository neighborhoodRepository;

    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private MunicipalityRepository municipalityRepository;

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DIRECTION = "TEST_DIRECTION";
    private static final String PHONE = "TEST_PHONE";
    private static final String MAIL = "TEST_MAIL";
    private static final String PASSWORD = "TEST_PASSWORD";
    private static final int ROLE_ID = 1;
    private static final int NEIGHBORHOOD_ID = 1;

    @Before
    public void setUpGetAll() {
        UserModel user1 = new UserModel();
        UserModel user2 = new UserModel();
        UserModel user3 = new UserModel();
        List<UserModel> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        Mockito.when(repository.findAll()).thenReturn(userList);
    }

    @Before
    public void setUpFindById() {
        UserModel user = new UserModel();
        user.setId(ID);
        Optional<UserModel> optionalUser = Optional.of(user);

        Mockito.when(repository.findById(user.getId()))
                .thenReturn(optionalUser);
    }

    @Before
    public void setUpCreate() {
        UserModel user = new UserModel();
        UserModel found = new UserModel();
        found.setName(NAME);
        found.setName(DIRECTION);
        Mockito.when(repository.save(user)).thenReturn(found);
    }

    @Before
    public void setUpLogOut(){
        UserModel user = new UserModel();

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name",NAME);
        obj.put("password",PASSWORD);
        obj.put("direction",DIRECTION);
        obj.put("phone",PHONE);
        obj.put("mail",MAIL);
        obj.put("role",ROLE_ID);
        obj.put("neighborhood",NEIGHBORHOOD_ID);

        Mockito.when(service.create(obj)).thenReturn(user);
    }

    @Before
    public void setUpLogIn(){
        UserModel user = new UserModel();

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name",NAME);
        obj.put("password",PASSWORD);
        obj.put("direction",DIRECTION);
        obj.put("phone",PHONE);
        obj.put("mail",MAIL);
        obj.put("role",ROLE_ID);
        obj.put("neighborhood",NEIGHBORHOOD_ID);

        Mockito.when(service.create(obj)).thenReturn(user);
    }

    @Test
    public void getAll() {
        List<UserModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        UserModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("name","TEST_NAME");
        obj.put("direction","TEST_DIRECTION");
        obj.put("phone","TEST_PHONE");
        obj.put("mail","TEST_MAIL");
        obj.put("password","TEST_PASSWORD");
        obj.put("role",1);
        obj.put("neighborhood",1);

        UserModel found = service.create(obj);
        Assertions.assertThat(found.getName()).isEqualTo(NAME);
    }

    @Test
    public void delete() {
        UserModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

}