package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.repositories.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class AnnouncementServiceTest {

    @TestConfiguration
    static class AnnouncementServiceTestContextConfiguration {
        @Bean
        public AnnouncementService announcementService() {
            return new AnnouncementService();
        }
    }

    @Autowired
    private AnnouncementService service;

    @MockBean
    private AnnouncementRepository repository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    // Test Values
    private static final int ID = 1;
    private static final String TITLE = "TEST_TITLE";
    private static final String DESCRIPTION = "TEST_DESCRIPTION";
    private static final String DATE = "TEST_DATE";
    private static final int PRICE = 0;
    private static final int APRECIATION = 0;
    private static final int SHOWN = 0;
    private static final String CONTACT = "TEST_CONTACT";
    private static final int USER_ID = 1;
    private static final int CATEGORY_ID = 1;

    @Before
    public void setUpGetAll() {
        AnnouncementModel announcement1 = new AnnouncementModel();
        AnnouncementModel announcement2 = new AnnouncementModel();
        AnnouncementModel announcement3 = new AnnouncementModel();
        List<AnnouncementModel> announcementList = new ArrayList<>();
        announcementList.add(announcement1);
        announcementList.add(announcement2);
        announcementList.add(announcement3);
        Mockito.when(repository.findAll()).thenReturn(announcementList);
    }

    @Before
    public void setUpFindById() {
        AnnouncementModel announcement = new AnnouncementModel();
        announcement.setId(ID);
        Optional<AnnouncementModel> optionalAnnouncement = Optional.of(announcement);

        Mockito.when(repository.findById(announcement.getId()))
                .thenReturn(optionalAnnouncement);
    }

    @Before
    public void setUpCreate() {
        AnnouncementModel announcement = new AnnouncementModel();
        AnnouncementModel found = new AnnouncementModel();
        found.setTitle(TITLE);
        found.setDescription(DESCRIPTION);
        Mockito.when(repository.save(announcement)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<AnnouncementModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        AnnouncementModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("title",TITLE);
        obj.put("description",DESCRIPTION);
        obj.put("date",DATE);
        obj.put("price",PRICE);
        obj.put("apreciation",APRECIATION);
        obj.put("shown",0);
        obj.put("contact",CONTACT);

        String found = service.create(obj);
        Assertions.assertThat(found).isEqualTo("Ha ocurrido un problema ingresando el anuncio");
    }

    @Test
    public void delete() {
        AnnouncementModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void getAllByUserRole() {
        JSONObject obj = new JSONObject();
        obj.put("token",TITLE);

        List<AnnouncementModel> found = service.getAllByUserRole(obj);
        Assertions.assertThat(found.size()).isZero();
    }

    @Test
    public void getAllByCategory() {
        JSONObject obj = new JSONObject();
        obj.put("token",TITLE);
        obj.put("category","Venta de Automoviles");

        List<AnnouncementModel> found = service.getAllByCategory(obj);
        Assertions.assertThat(found.size()).isZero();
    }
}