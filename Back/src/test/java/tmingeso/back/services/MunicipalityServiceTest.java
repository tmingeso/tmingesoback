package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.repositories.MunicipalityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class MunicipalityServiceTest {

    @TestConfiguration
    static class MunicipalityServiceTestContextConfiguration {
        @Bean
        public MunicipalityService municipalityService() {
            return new MunicipalityService();
        }
    }

    @Autowired
    private MunicipalityService service;

    @MockBean
    private MunicipalityRepository repository;

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String MAYOR = "TEST_MAYOR";

    @Before
    public void setUpGetAll() {
        MunicipalityModel municipality1 = new MunicipalityModel();
        MunicipalityModel municipality2 = new MunicipalityModel();
        MunicipalityModel municipality3 = new MunicipalityModel();
        List<MunicipalityModel> municipalityList = new ArrayList<>();
        municipalityList.add(municipality1);
        municipalityList.add(municipality2);
        municipalityList.add(municipality3);
        Mockito.when(repository.findAll()).thenReturn(municipalityList);
    }

    @Before
    public void setUpFindById() {
        MunicipalityModel municipality = new MunicipalityModel();
        municipality.setId(ID);
        Optional<MunicipalityModel> optionalMunicipalityModel = Optional.of(municipality);

        Mockito.when(repository.findById(municipality.getId()))
                .thenReturn(optionalMunicipalityModel);
    }

    @Before
    public void setUpCreate() {
        MunicipalityModel municipality = new MunicipalityModel();
        MunicipalityModel found = new MunicipalityModel();
        found.setName(NAME);
        found.setMayor(MAYOR);
        Mockito.when(repository.save(municipality)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<MunicipalityModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        MunicipalityModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("name","TEST_NAME");
        obj.put("mayor","TEST_MAYOR");

        MunicipalityModel found = service.create(obj);
        Assertions.assertThat(found.getName()).isEqualTo(NAME);
    }

    @Test
    public void delete() {
        MunicipalityModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }
}