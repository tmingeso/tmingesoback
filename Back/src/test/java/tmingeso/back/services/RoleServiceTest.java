package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.RoleModel;
import tmingeso.back.repositories.RoleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class RoleServiceTest {

    @TestConfiguration
    static class RoleServiceTestContextConfiguration {
        @Bean
        public RoleService roleService() {
            return new RoleService();
        }
    }

    @Autowired
    private RoleService service;

    @MockBean
    private RoleRepository repository;

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DESCRIPTION = "TEST_DESCRIPTION";

    @Before
    public void setUpGetAll() {
        RoleModel role1 = new RoleModel();
        RoleModel role2 = new RoleModel();
        RoleModel role3 = new RoleModel();
        List<RoleModel> roleList = new ArrayList<>();
        roleList.add(role1);
        roleList.add(role2);
        roleList.add(role3);
        Mockito.when(repository.findAll()).thenReturn(roleList);
    }

    @Before
    public void setUpFindById() {
        RoleModel role = new RoleModel();
        role.setId(ID);
        Optional<RoleModel> optionalContactForm = Optional.of(role);

        Mockito.when(repository.findById(role.getId()))
                .thenReturn(optionalContactForm);
    }

    @Before
    public void setUpCreate() {
        RoleModel contactForm = new RoleModel();
        RoleModel found = new RoleModel();
        found.setName(NAME);
        found.setName(DESCRIPTION);
        Mockito.when(repository.save(contactForm)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<RoleModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        RoleModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("name","TEST_NAME");
        obj.put("description","TEST_DESCRIPTION");

        RoleModel found = service.create(obj);
        Assertions.assertThat(found.getName()).isEqualTo(NAME);
    }

    @Test
    public void delete() {
        RoleModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }
}