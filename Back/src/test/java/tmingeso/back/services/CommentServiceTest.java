package tmingeso.back.services;

import net.minidev.json.JSONObject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import tmingeso.back.models.CommentModel;
import tmingeso.back.repositories.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
public class CommentServiceTest {

    @TestConfiguration
    static class CommentServiceTestContextConfiguration {
        @Bean
        public CommentService commentService() {
            return new CommentService();
        }
    }

    @Autowired
    private CommentService service;

    @MockBean
    private CommentRepository repository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private AnnouncementRepository announcementRepository;

    // Test Values
    private static final int ID = 1;
    private static final int USER_ID = 1;
    private static final int ANNOUNCEMENT_ID = 1;
    private static final String COMMENT = "TEST_COMMENT";

    @Before
    public void setUpGetAll() {
        CommentModel comment1 = new CommentModel();
        CommentModel comment2 = new CommentModel();
        CommentModel comment3 = new CommentModel();
        List<CommentModel> commentList = new ArrayList<>();
        commentList.add(comment1);
        commentList.add(comment2);
        commentList.add(comment3);
        Mockito.when(repository.findAll()).thenReturn(commentList);
    }

    @Before
    public void setUpGetAllByAnnouncementId() {
        CommentModel comment1 = new CommentModel();
        CommentModel comment2 = new CommentModel();
        CommentModel comment3 = new CommentModel();
        List<CommentModel> commentList = new ArrayList<>();
        commentList.add(comment1);
        commentList.add(comment2);
        commentList.add(comment3);
        Mockito.when(repository.findAll()).thenReturn(commentList);
    }

    @Before
    public void setUpFindById() {
        CommentModel comment = new CommentModel();
        comment.setId(ID);
        Optional<CommentModel> optionalCommentModel = Optional.of(comment);

        Mockito.when(repository.findById(comment.getId()))
                .thenReturn(optionalCommentModel);
    }

    @Before
    public void setUpCreate() {
        CommentModel comment = new CommentModel();
        CommentModel found = new CommentModel();
        found.setId(ID);
        found.setComment(COMMENT);
        found.setAnnouncement(announcementRepository.findAnnouncementModelById(ANNOUNCEMENT_ID));
        found.setUser(userRepository.findUserModelById(USER_ID));

        Mockito.when(repository.save(comment)).thenReturn(found);
    }

    @Test
    public void getAll() {
        List<CommentModel> found = service.getAll();
        Assertions.assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void getById() {
        CommentModel found = service.getById(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void create() {

        JSONObject obj = new JSONObject();
        obj.put("token","TEST_NAME");
        obj.put("comment",COMMENT);

        String found = service.create(obj);
        Assertions.assertThat(found).isEqualTo("Ha ocurrido un problema ingresando el comentario");
    }

    @Test
    public void delete() {
        CommentModel found = service.delete(ID);
        Assertions.assertThat(found.getId()).isEqualTo(ID);
    }

    @Test
    public void getAllByAnnouncementId() {
        JSONObject obj = new JSONObject();
        obj.put("announcementId",ANNOUNCEMENT_ID);
        List<CommentModel> found = service.getAllByAnnouncementId(obj);
        Assertions.assertThat(found.size()).isZero();
    }

}