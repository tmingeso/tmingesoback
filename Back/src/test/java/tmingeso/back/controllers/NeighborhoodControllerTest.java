package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.NeighborhoodModel;
import tmingeso.back.models.RoleModel;
import tmingeso.back.services.MunicipalityService;
import tmingeso.back.services.NeighborhoodService;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(NeighborhoodController.class)
@AutoConfigureDataJpa
public class NeighborhoodControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private NeighborhoodService service;

    @MockBean
    private MunicipalityService municipalityService;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/neighborhood";

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DIRECTION = "TEST_DIRECTION";
    private static final String PHONE = "TEST_PHONE";
    private static final String MAIL = "TEST_MAIL";
    private static final int MUNICIPALITY_ID = 1;

    @Test
    public void getAll() {
        // Declarations
        NeighborhoodModel neighborhood = new NeighborhoodModel();
        List<NeighborhoodModel> neighborhoodList = new ArrayList<>();

        // Sets and Adds
        neighborhood.setNeighborhoodName(NAME);
        neighborhood.setNeighborhoodDirection(DIRECTION);
        neighborhood.setNeighborhoodMail(MAIL);
        neighborhood.setNeighborhoodPhone(PHONE);
        neighborhoodList.add(neighborhood);

        // Test
        given(service.getAll()).willReturn(neighborhoodList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].neighborhoodName",
                            is(NAME)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        NeighborhoodModel neighborhood = new NeighborhoodModel();

        // Sets and Adds
        neighborhood.setId(ID);
        neighborhood.setNeighborhoodName(NAME);
        neighborhood.setNeighborhoodDirection(DIRECTION);

        // Test
        given(service.getById(ID)).willReturn(neighborhood);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        NeighborhoodModel neighborhood = new NeighborhoodModel();

        // Sets and Adds
        neighborhood.setId(ID);
        neighborhood.setNeighborhoodName(NAME);
        neighborhood.setNeighborhoodDirection(DIRECTION);
        neighborhood.setNeighborhoodPhone(PHONE);
        neighborhood.setNeighborhoodMail(MAIL);
        neighborhood.setMunicipality(municipalityService.getById(MUNICIPALITY_ID));

        JSONObject obj = new JSONObject();
        obj.put("id",ID);
        obj.put("name","TEST_NAME");
        obj.put("direction","TEST_DIRECTION");
        obj.put("phone","TEST_PHONE");
        obj.put("mail","TEST_MAIL");
        obj.put("municipality",1);

        // Test
        given(service.create(obj)).willReturn(neighborhood);
        try {
            mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(neighborhood.getId())))
                    .andExpect(jsonPath("$.neighborhoodName",
                            is(neighborhood.getNeighborhoodName())))
                    .andExpect(jsonPath("$.neighborhoodDirection",
                            is(neighborhood.getNeighborhoodDirection())));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        NeighborhoodModel neighborhood = new NeighborhoodModel();

        // Sets and Adds
        neighborhood.setId(ID);
        neighborhood.setNeighborhoodName(NAME);
        neighborhood.setNeighborhoodDirection(DIRECTION);

        // Test
        given(service.delete(neighborhood.getId())).willReturn(neighborhood);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}