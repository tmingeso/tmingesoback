package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.apache.catalina.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.RoleModel;
import tmingeso.back.models.UserModel;
import tmingeso.back.services.RoleService;
import tmingeso.back.services.UserService;

import java.awt.desktop.UserSessionListener;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@AutoConfigureDataJpa
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService service;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/user";

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DIRECTION = "TEST_DIRECTION";
    private static final String PHONE = "TEST_PHONE";
    private static final String MAIL = "TEST_MAIL";
    private static final String PASSWORD = "TEST_PASSWORD";
    private static final int ROLE_ID = 1;
    private static final int NEIGHBORHOOD_ID = 1;

    @Test
    public void getAll() {
        // Declarations
        UserModel user = new UserModel();
        List<UserModel> userList = new ArrayList<>();

        // Sets and Adds
        user.setName(NAME);
        user.setDirection(DIRECTION);
        userList.add(user);

        // Test
        given(service.getAll()).willReturn(userList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].name",
                            is(NAME)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        UserModel user = new UserModel();

        // Sets and Adds
        user.setId(ID);
        user.setName(NAME);
        user.setDirection(DIRECTION);

        // Test
        given(service.getById(ID)).willReturn(user);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        UserModel user = new UserModel();

        // Sets and Adds
        user.setId(ID);
        user.setName(NAME);
        user.setDirection(DIRECTION);

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name","TEST_NAME");
        obj.put("description","TEST_DESCRIPTION");

        // Test
        given(service.create(obj)).willReturn(user);
        try {
            mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(user.getId())))
                    .andExpect(jsonPath("$.name",
                            is(user.getName())))
                    .andExpect(jsonPath("$.direction",
                            is(user.getDirection())));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        UserModel user = new UserModel();

        // Sets and Adds
        user.setId(ID);
        user.setName(NAME);
        user.setDirection(DIRECTION);

        // Test
        given(service.delete(user.getId())).willReturn(user);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}