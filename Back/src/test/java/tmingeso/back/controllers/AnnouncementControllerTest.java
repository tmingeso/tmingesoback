package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.services.AnnouncementService;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(AnnouncementController.class)
@AutoConfigureDataJpa
public class AnnouncementControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnnouncementService service;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/announcement";

    // Test Values
    private static final int ID = 1;
    private static final String TITLE = "TEST_TITLE";
    private static final String DESCRIPTION = "TEST_DESCRIPTION";
    private static final String DATE = "TEST_DATE";
    private static final int PRICE = 0;
    private static final int APRECIATION = 0;
    private static final int SHOWN = 0;
    private static final String CONTACT = "TEST_CONTACT";
    private static final int USER_ID = 1;
    private static final int CATEGORY_ID = 1;

    @Test
    public void getAll() {
        // Declarations
        AnnouncementModel announcement = new AnnouncementModel();
        List<AnnouncementModel> announcementList = new ArrayList<>();

        // Sets and Adds
        announcement.setTitle(TITLE);
        announcement.setDescription(DESCRIPTION);
        announcementList.add(announcement);

        // Test
        given(service.getAll()).willReturn(announcementList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].title",
                            is(TITLE)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        AnnouncementModel announcement = new AnnouncementModel();

        // Sets and Adds
        announcement.setId(ID);
        announcement.setTitle(TITLE);
        announcement.setDescription(DESCRIPTION);

        // Test
        given(service.getById(ID)).willReturn(announcement);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        AnnouncementModel announcement = new AnnouncementModel();
        String verify = "Anuncio publicado con exito!";

        // Sets and Adds
        announcement.setId(ID);
        announcement.setTitle(TITLE);
        announcement.setDescription(DESCRIPTION);
        announcement.setDate(DATE);
        announcement.setPrice(PRICE);
        announcement.setApreciation(APRECIATION);
        announcement.setShown(0);
        announcement.setContact(CONTACT);

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("title",TITLE);
        obj.put("description",DESCRIPTION);
        obj.put("date",DATE);
        obj.put("price",PRICE);
        obj.put("apreciation",APRECIATION);
        obj.put("shown",0);
        obj.put("contact",CONTACT);


        // Test
        given(service.create(obj)).willReturn(verify);
        try {
            MvcResult result =  mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(status().isOk()).andReturn();
            String content = result.getResponse().getContentAsString();
            content.equals(verify);


        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        AnnouncementModel announcement = new AnnouncementModel();

        // Sets and Adds
        announcement.setId(ID);
        announcement.setTitle(TITLE);
        announcement.setDescription(DESCRIPTION);

        // Test
        given(service.delete(announcement.getId())).willReturn(announcement);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}