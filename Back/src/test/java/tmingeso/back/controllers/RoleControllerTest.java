package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.RoleModel;
import tmingeso.back.services.RoleService;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(RoleController.class)
@AutoConfigureDataJpa
public class RoleControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RoleService service;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/role";

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String DESCRIPTION = "TEST_DESCRIPTION";

    @Test
    public void getAll() {
        // Declarations
        RoleModel role = new RoleModel();
        List<RoleModel> roleList = new ArrayList<>();

        // Sets and Adds
        role.setName(NAME);
        role.setDescription(DESCRIPTION);
        roleList.add(role);

        // Test
        given(service.getAll()).willReturn(roleList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].name",
                            is(NAME)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        RoleModel role = new RoleModel();

        // Sets and Adds
        role.setId(ID);
        role.setName(NAME);
        role.setDescription(DESCRIPTION);

        // Test
        given(service.getById(ID)).willReturn(role);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        RoleModel role = new RoleModel();

        // Sets and Adds
        role.setId(ID);
        role.setName(NAME);
        role.setDescription(DESCRIPTION);

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name","TEST_NAME");
        obj.put("description","TEST_DESCRIPTION");

        // Test
        given(service.create(obj)).willReturn(role);
        try {
            mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(role.getId())))
                    .andExpect(jsonPath("$.name",
                            is(role.getName())))
                    .andExpect(jsonPath("$.description",
                            is(role.getDescription())));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        RoleModel role = new RoleModel();

        // Sets and Adds
        role.setId(ID);
        role.setName(NAME);
        role.setDescription(DESCRIPTION);

        // Test
        given(service.delete(role.getId())).willReturn(role);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}