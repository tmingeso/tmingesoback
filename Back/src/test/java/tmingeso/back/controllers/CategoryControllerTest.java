package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.CategoryModel;
import tmingeso.back.services.CategoryService;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
@AutoConfigureDataJpa
public class CategoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CategoryService service;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/category";

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";

    @Test
    public void getAll() {
        // Declarations
        CategoryModel category = new CategoryModel();
        List<CategoryModel> categoryList = new ArrayList<>();

        // Sets and Adds
        category.setName(NAME);
        categoryList.add(category);

        // Test
        given(service.getAll()).willReturn(categoryList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + "all")
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].name",
                            is(NAME)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        CategoryModel category = new CategoryModel();

        // Sets and Adds
        category.setId(ID);
        category.setName(NAME);

        // Test
        given(service.getById(ID)).willReturn(category);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        CategoryModel category = new CategoryModel();

        // Sets and Adds
        category.setId(ID);
        category.setName(NAME);

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name","TEST_NAME");

        // Test
        given(service.create(obj)).willReturn(category);
        try {
            mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(category.getId())))
                    .andExpect(jsonPath("$.name",
                            is(category.getName())));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        CategoryModel category = new CategoryModel();

        // Sets and Adds
        category.setId(ID);
        category.setName(NAME);

        // Test
        given(service.delete(category.getId())).willReturn(category);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}