package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.CommentModel;
import tmingeso.back.services.AnnouncementService;
import tmingeso.back.services.CommentService;
import tmingeso.back.services.UserService;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@AutoConfigureDataJpa
public class CommentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CommentService service;
    @MockBean
    private AnnouncementService announcementService;
    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/comment";

    // Test Values
    private static final int ID = 1;
    private static final int USER_ID = 1;
    private static final int ANNOUNCEMENT_ID = 1;
    private static final String COMMENT = "TEST_COMMENT";;

    @Test
    public void getAll() {
        // Declarations
        CommentModel comment = new CommentModel();
        List<CommentModel> commentList = new ArrayList<>();

        // Sets and Adds
        comment.setComment(COMMENT);
        comment.setAnnouncement(announcementService.getById(ANNOUNCEMENT_ID));
        comment.setUser(userService.getById(USER_ID));
        commentList.add(comment);

        // Test
        given(service.getAll()).willReturn(commentList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].comment",
                            is(COMMENT)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        CommentModel comment = new CommentModel();

        // Sets and Adds
        comment.setId(ID);
        comment.setComment(COMMENT);
        comment.setAnnouncement(announcementService.getById(ANNOUNCEMENT_ID));
        comment.setUser(userService.getById(USER_ID));

        // Test
        given(service.getById(ID)).willReturn(comment);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        CommentModel comment = new CommentModel();
        String verify = "Comentario publicado con exito.";

        // Sets and Adds
        comment.setId(ID);
        comment.setComment(COMMENT);
        comment.setAnnouncement(announcementService.getById(ANNOUNCEMENT_ID));
        comment.setUser(userService.getById(USER_ID));

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("comment",COMMENT);
        obj.put("announcementId",ANNOUNCEMENT_ID);

        // Test
        given(service.create(obj)).willReturn(verify);
        try {
            MvcResult result =  mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(status().isOk()).andReturn();
            String content = result.getResponse().getContentAsString();
            content.equals(verify);
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        CommentModel comment = new CommentModel();

        // Sets and Adds
        comment.setId(ID);
        comment.setComment(COMMENT);
        comment.setAnnouncement(announcementService.getById(ANNOUNCEMENT_ID));
        comment.setUser(userService.getById(USER_ID));

        // Test
        given(service.delete(comment.getId())).willReturn(comment);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}