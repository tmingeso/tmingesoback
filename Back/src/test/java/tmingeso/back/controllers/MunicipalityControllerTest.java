package tmingeso.back.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.services.MunicipalityService;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(MunicipalityController.class)
@AutoConfigureDataJpa
public class MunicipalityControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MunicipalityService service;

    @Autowired
    private ObjectMapper mapper;

    // URL
    private static final String URL = "/municipality";

    // Test Values
    private static final int ID = 1;
    private static final String NAME = "TEST_NAME";
    private static final String MAYOR = "TEST_MAYOR";

    @Test
    public void getAll() {
        // Declarations
        MunicipalityModel municipality = new MunicipalityModel();
        List<MunicipalityModel> municipalityList = new ArrayList<>();

        // Sets and Adds
        municipality.setName(NAME);
        municipality.setMayor(MAYOR);
        municipalityList.add(municipality);

        // Test
        given(service.getAll()).willReturn(municipalityList);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$",
                            hasSize(1)))
                    .andExpect(jsonPath("$[0].name",
                            is(NAME)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void getById() {
        // Declarations
        MunicipalityModel municipality = new MunicipalityModel();

        // Sets and Adds
        municipality.setId(ID);
        municipality.setName(NAME);
        municipality.setMayor(MAYOR);

        // Test
        given(service.getById(ID)).willReturn(municipality);
        try {
            mvc.perform(MockMvcRequestBuilders.get(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void create() {
        // Declarations
        MunicipalityModel municipality = new MunicipalityModel();

        // Sets and Adds
        municipality.setId(ID);
        municipality.setName(NAME);
        municipality.setMayor(MAYOR);

        JSONObject obj = new JSONObject();
        obj.put("id",1);
        obj.put("name","TEST_NAME");
        obj.put("mayor","TEST_MAYOR");

        // Test
        given(service.create(obj)).willReturn(municipality);
        try {
            mvc.perform(MockMvcRequestBuilders.post(URL)
                    .contentType(APPLICATION_JSON)
                    .content(mapper.writeValueAsString(obj)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(municipality.getId())))
                    .andExpect(jsonPath("$.name",
                            is(municipality.getName())))
                    .andExpect(jsonPath("$.mayor",
                            is(municipality.getMayor())));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void delete() {
        // Declarations
        MunicipalityModel municipality = new MunicipalityModel();

        // Sets and Adds
        municipality.setId(ID);
        municipality.setName(NAME);
        municipality.setMayor(MAYOR);

        // Test
        given(service.delete(municipality.getId())).willReturn(municipality);
        try {
            mvc.perform(MockMvcRequestBuilders.delete(URL + "/" + ID)
                    .contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",
                            is(ID)));
        } catch (Exception ignored) {
        }
    }

}