INSERT comment (id, comment, announcement_id, user_id)
    VALUES
        (1, "Comentario de prueba.", 26, 7),
        (2, "En qué condiciones se encuentra?", 1, 8),
        (3, "¿Tiene los papeles al día?", 1, 9),
        (4, "Cambian partes de computadores?", 3, 10),
        (5, "Todavía no tienen listo mi notebook", 3, 11),
        (6, "Hacen muebles a medida?", 8, 12),
        (7, "Cuando se realizó la última mantención?", 2, 13),
        (8, "Hacen precios por mayor?", 5, 14),
        (9, "Necesito delivery.", 5, 15),
        (10, "Aceptas 2500000?", 12, 16)