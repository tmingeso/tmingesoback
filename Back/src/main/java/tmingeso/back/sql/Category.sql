INSERT category (id,name)
    VALUES
        (1, "Venta de Ropa"),
        (2, "Venta de Inmobilaria"),
        (3, "Venta de Automovil"),
        (4, "Venta de Comida"),
        (5, "Servicio de Delivery"),
        (6, "Servicio de Mudanza"),
        (7, "Servicio de Construcción"),
        (8, "Servicios Informáticos"),
        (9, "Servicios Electronicos"),
        (10, "Asistencia Tercera Edad")