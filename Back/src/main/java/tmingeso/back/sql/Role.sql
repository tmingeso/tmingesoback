INSERT role (id,description,name)
    VALUES
        (1,"Sirve de administrador para las juntas de vecinos de la comuna, este las visualiza y las administra.","Usuario Municipalidad"),
        (2,"Sirve de administrador para la junta de vecino respectiva, este visualiza y administra dicha junta","Usuario Representante Junta de Vecinos"),
        (3,"Usuario natural, este pertenece a una junta de vecino y es capaz de publicar anuncios, comentar anuncion y valorar anuncios. También puede visualizar los avisos de su propio sector","Usuario Ciudadano")