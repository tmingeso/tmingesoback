INSERT announcement (id,apreciation,date,description,price,shown,title,category_id,user_id)
    VALUES
        (1,0,"31/12/2020","Se vende auto Toyota Yaris por modificación de automovil.",1000000,1,"Venta Auto Toyota Yaris 1997.",3,7),
        (2,0,"20/12/2020","Se vende auto Toyota Yaris por modificación de automovil.",3500000,1,"Venta Auto Toyota Yaris 2010.",3,7),
        (3,0,"20/12/2020","Se proveen distintos servicios de mantención de productos Informáticos.",10000,1,"Servicios de Mantención Informáticos.",8,8),
        (4,0,"20/12/2020","Se proveen distintos servicios de mantención de productos Eléctricos.",10000,1,"Servicios de Mantención Eléctricos.",9,8),
        (5,0,"20/12/2020","Se venden distintos tipos de pizza dentro de nuestra comunidad.",12000,1,"Pizzería Vecinal.",4,9),
        (6,0,"20/12/2020","Se venden distintos tipos de cómida rapida, contamos con delivery!.",5000,1,"Comida Rápida.",4,9),
        (7,0,"20/12/2020","Se venden ropa nueva y usada, tenemos todas las tallas.",5000,1,"Tienda de Ropa",1,10),
        (8,0,"20/12/2020","Se venden muebles, tenemos muebles ya fabricados o hacemos a medida.",50000,1,"Tienda de Muebles",2,10),
        (9,0,"20/12/2020","Se proveen distintos servicios de mantención de productos Eléctricos.",10000,1,"Servicios de Mantención Eléctricos.",9,11),
        (10,0,"20/12/2020","Se proveen distintos servicios de mantención de productos informáticos.",10000,1,"Servicios de Mantención Informáticos.",8,11),
        (11,0,"31/12/2020","Se vende auto Toyota Yaris por modificación de automovil.",1000000,1,"Venta Auto Toyota Yaris 1997.",3,13),
        (12,0,"31/12/2020","Se vende camioneta para realizar tareas de carga y/o delivery.",3000000,1,"Venta Camioneta 3/4 buenas condiciones.",3,13),
        (13,0,"20/12/2020","Se vende auto Volkswagen Golf del ao 1997 por no uso, funciona correctamente.",1000000,1,"Se vende Volkswagen Golf 1997",3,16),
        (14,0,"20/12/2020","Se vende auto Volkswagen Golf del ao 2010 por no uso, funciona correctamente.",5000000,1,"Se vende Volkswagen Golf 2010",3,16),
        (15,0,"20/12/2020","Se presta ayuda a personas de la tercera edad que tengan dificultad al momento de realizar el tramite del bono online.",0,1,"Ayuda a tercera edad para retiro de bono del estado",10,17),
        (16,0,"20/12/2020","Se prestan servicios de delivery dentro del sector, contamos con motocicleta para un envío rápido y contamos con  un camion para realizar servicios más pesados",10000,1,"Se hacen envios dentro del sector",5,17)