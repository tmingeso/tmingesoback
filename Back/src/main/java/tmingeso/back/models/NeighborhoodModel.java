package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Neighborhood")
public class NeighborhoodModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String neighborhoodName;

    @Column(name="direction")
    private String neighborhoodDirection;

    @Column(name="phone")
    private String neighborhoodPhone;

    @Column(name="mail")
    private String neighborhoodMail;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "municipalityId")
    private MunicipalityModel municipality;

    public void setNeighborhood(NeighborhoodModel newNeighborhood, MunicipalityModel municipality){
        this.neighborhoodName = newNeighborhood.getNeighborhoodName();
        this.neighborhoodDirection = newNeighborhood.getNeighborhoodDirection();
        this.neighborhoodPhone = newNeighborhood.getNeighborhoodPhone();
        this.neighborhoodMail = newNeighborhood.getNeighborhoodMail();
        this.municipality = municipality;

    }

}
