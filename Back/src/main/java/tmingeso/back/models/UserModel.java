package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "User")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name="direction")
    private String direction;

    @Column(name="phone")
    private String phone;

    @Column(name="mail")
    private String mail;

    @Column(name="salt")
    private byte[] salt;

    @Column(name="password")
    private String password;

    @Column(name="loginToken")
    private String loginToken;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "roleId")
    private RoleModel role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "neighborhoodId")
    private NeighborhoodModel neighborhood;

    public void setUser(UserModel newUser, RoleModel role, NeighborhoodModel neighborhood){
        this.name = newUser.getName();
        this.direction = newUser.getDirection();
        this.phone = newUser.getPhone();
        this.mail = newUser.getMail();
        this.salt = newUser.getSalt();
        this.password = newUser.getPassword();
        this.loginToken = newUser.getLoginToken();
        this.role = role;
        this.neighborhood = neighborhood;

    }

}
