package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Role")
public class RoleModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name="description")
    private String description;

    public void setRole(RoleModel newRole){
        this.name = newRole.getName();
        this.description = newRole.getDescription();
    }

}

