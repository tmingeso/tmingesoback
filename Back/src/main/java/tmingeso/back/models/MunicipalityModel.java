package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Municipality")
public class MunicipalityModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name="mayor")
    private String mayor;

    public void setMunicipality(MunicipalityModel newMunicipality){
        this.name = newMunicipality.getName();
        this.mayor = newMunicipality.getMayor();
    }

}