package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Comment")
public class CommentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private UserModel user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "announcementId")
    private AnnouncementModel announcement;


    public void setComment(CommentModel newComment, UserModel user, AnnouncementModel announcement){
        this.comment = newComment.getComment();
        this.user = user;
        this.announcement = announcement;
    }

}
