package tmingeso.back.models;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Announcement")
public class AnnouncementModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name="description")
    private String description;

    @Column(name="date")
    private String date;

    @Column(name="price")
    private int price;

    @Column(name="apreciation")
    private int apreciation;

    @Column(name="shown")
    private int shown;

    @Column(name="contact")
    private String contact;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private UserModel user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "categoryId", nullable = false)
    private CategoryModel category;


    public void setAnnouncement(AnnouncementModel newAnnouncement, UserModel user){
        this.title = newAnnouncement.getTitle();
        this.description = newAnnouncement.getDescription();
        this.date = newAnnouncement.getDate();
        this.price = newAnnouncement.getPrice();
        this.apreciation = newAnnouncement.getApreciation();
        this.shown = newAnnouncement.getShown();
        this.contact = newAnnouncement.getContact();
        this.user = user;
        this.category = newAnnouncement.getCategory();
    }

}
