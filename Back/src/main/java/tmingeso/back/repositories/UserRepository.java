package tmingeso.back.repositories;

import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
    Optional<UserModel> findById(int id);
    UserModel findUserModelById(int id);
    UserModel findUserModelByLoginToken(String loginToken);
    UserModel findByName(String name);
    List<UserModel> findUserModelByNeighborhoodMunicipalityAndAndRoleId(MunicipalityModel municipality, int id);
}
