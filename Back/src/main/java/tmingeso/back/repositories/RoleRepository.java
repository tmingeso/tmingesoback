package tmingeso.back.repositories;

import tmingeso.back.models.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Integer> {
    Optional<RoleModel> findById(int id);
    RoleModel findRoleModelById(int id);
}