package tmingeso.back.repositories;

import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.models.CategoryModel;
import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.models.NeighborhoodModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnnouncementRepository extends JpaRepository<AnnouncementModel, Integer> {
    Optional<AnnouncementModel> findById(int id);
    AnnouncementModel findAnnouncementModelById(int id);
    List<AnnouncementModel> findAnnouncementModelsByUserNeighborhood(NeighborhoodModel neighborhood);
    List<AnnouncementModel> findAnnouncementModelsByUserNeighborhoodMunicipality(MunicipalityModel municipality);
    List<AnnouncementModel> findAnnouncementModelsByUserNeighborhoodAndCategory(NeighborhoodModel neighborhood, CategoryModel category);
    List<AnnouncementModel> findAnnouncementModelsByUserNeighborhoodMunicipalityAndCategory(MunicipalityModel municipality,CategoryModel category);
}