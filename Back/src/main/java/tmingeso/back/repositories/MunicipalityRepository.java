package tmingeso.back.repositories;

import tmingeso.back.models.MunicipalityModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface MunicipalityRepository extends JpaRepository<MunicipalityModel, Integer> {
    Optional<MunicipalityModel> findById(int id);
    MunicipalityModel findMunicipalityModelById(int id);
}