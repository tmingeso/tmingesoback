package tmingeso.back.repositories;

import tmingeso.back.models.NeighborhoodModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NeighborhoodRepository extends JpaRepository<NeighborhoodModel, Integer> {
    Optional<NeighborhoodModel> findById(int id);
    NeighborhoodModel findNeighborhoodModelById(int id);
}
