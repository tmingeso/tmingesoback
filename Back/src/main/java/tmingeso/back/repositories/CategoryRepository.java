package tmingeso.back.repositories;

import tmingeso.back.models.CategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryModel, Integer> {
    Optional<CategoryModel> findById(int id);
    CategoryModel findCategoryModelByName(String name);
}