package tmingeso.back.services;

import net.minidev.json.JSONObject;
import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.models.UserModel;
import tmingeso.back.repositories.*;
import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@NoArgsConstructor
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private NeighborhoodRepository neighborhoodRepository;
    @Autowired
    private MunicipalityRepository municipalityRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<UserModel> getAll() {
        return userRepository.findAll();
    }

    public UserModel getById(int id) {
        return userRepository.findById(id)
                .orElse(null);
    }

    public UserModel create(@RequestBody Map<String, Object> jsonData) {

        UserModel user = new UserModel();

        user.setName(jsonData.get("name").toString());
        user.setDirection(jsonData.get("direction").toString());
        user.setPhone(jsonData.get("phone").toString());
        user.setMail(jsonData.get("mail").toString());
        user.setRole(roleRepository.findRoleModelById(Integer.valueOf(jsonData.get("role").toString())));
        user.setNeighborhood(neighborhoodRepository.findNeighborhoodModelById(Integer.valueOf(jsonData.get("neighborhood").toString())));

        SecureRandom secureRandom = new SecureRandom();
        byte[] salt = secureRandom.generateSeed(12);
        PBEKeySpec pbeKeySpec = new PBEKeySpec(jsonData.get("password").toString().toCharArray(), salt, 10, 512);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            byte[] hash = skf.generateSecret(pbeKeySpec).getEncoded();
            String base64Hash = Base64.getMimeEncoder().encodeToString(hash);
            user.setSalt(salt);
            user.setPassword(base64Hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        userRepository.save(user);

        return user;
    }

    public UserModel delete(int id) {
        return userRepository.findById(id)
                .map(userModel -> {
                    userRepository.delete(userModel);
                    return userModel;
                })
                .orElse(null);
    }

    public String logIn(@RequestBody Map<String, Object> jsonData) {

        UserModel user = userRepository.findByName(jsonData.get("name").toString());
        PBEKeySpec pbeKeySpec2 = new PBEKeySpec(jsonData.get("password").toString().toCharArray(), user.getSalt(), 10, 512);
        try {
            SecretKeyFactory skf2 = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            byte[] hash2 = skf2.generateSecret(pbeKeySpec2).getEncoded();
            String base64Hash2 = Base64.getMimeEncoder().encodeToString(hash2);
            if (base64Hash2.equals(user.getPassword())){
                UUID uuid = UUID.randomUUID();
                user.setLoginToken(uuid.toString());
                userRepository.save(user);
                return uuid.toString();
            }
            return "";
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String logOut(@RequestBody Map<String, Object> jsonData){

        Logger logger = Logger.getLogger(UserService.class.getName());

        try{
            UserModel user = userRepository.findUserModelByLoginToken(jsonData.get("token").toString());
            user.setLoginToken("");
            userRepository.save(user);
            return "El usuario se ha deslogueado correctamente!";
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "User does not exists!!",e);
        }
        return "Ha ocurrido un problema deslogueando al usuario especificado!";
    }

    public List<JSONObject> getUserRanking(@RequestBody Map<String, Object> jsonData){

        Logger logger = Logger.getLogger(UserService.class.getName());
        List<UserModel> userModelList = new ArrayList<>();
        List<Integer> commentsList = new ArrayList<>();
        List<JSONObject> usersList = new ArrayList<>();

        try {
            // User Logged.
            UserModel user = userRepository.findUserModelByLoginToken(jsonData.get("token").toString());
            // Municipality of logged user.
            MunicipalityModel municipality = municipalityRepository.findMunicipalityModelById(user.getNeighborhood().getMunicipality().getId());
            // User is Municipality.
            if(user.getRole().getId() == 1){
                userModelList = userRepository.findUserModelByNeighborhoodMunicipalityAndAndRoleId(municipality,3);
                for (int i = 0; i < userModelList.size(); i++){
                    commentsList.add((commentRepository.countCommentModelByUserId(userModelList.get(i).getId())));
                }

                int j;
                int i = 0;
                int aux = 0;
                UserModel userAux;

                while (i < commentsList.size()){
                    j = i + 1;
                    while (j < commentsList.size()){
                        if (commentsList.get(i) > commentsList.get(j)){
                            // Change in comment List.
                            aux = commentsList.get(i);
                            commentsList.set(i , commentsList.get(j));
                            commentsList.set(j , aux);
                            // Change in UserModel List.
                            userAux = userModelList.get(i);
                            userModelList.set(i , userModelList.get(j));
                            userModelList.set(j , userAux);
                        }
                        j += 1;
                    }
                    i +=1;
                }

                Collections.reverse(userModelList);
                Collections.reverse(commentsList);

                JSONObject obj = new JSONObject();

                for (int k = 0; k < userModelList.size(); k ++){

                    obj.put("name",userModelList.get(k).getName());
                    obj.put("phone",userModelList.get(k).getPhone());
                    obj.put("neighborhoodName",userModelList.get(k).getNeighborhood().getNeighborhoodName());
                    obj.put("number",commentsList.get(k).toString());

                    usersList.add(obj);
                    obj = new JSONObject();
                }

                return usersList;
            }
            // El usuario no es Municipalidad.
            else {
                return usersList;
            }
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "User does not exists!!",e);
        }

        return usersList;
    }
}
