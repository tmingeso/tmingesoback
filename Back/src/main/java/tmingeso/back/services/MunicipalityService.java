package tmingeso.back.services;

import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.repositories.MunicipalityRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


@Service
@NoArgsConstructor
public class MunicipalityService {

    @Autowired
    private MunicipalityRepository municipalityRepository;

    public List<MunicipalityModel> getAll() {
        return municipalityRepository.findAll();
    }

    public MunicipalityModel getById(int id) {
        return municipalityRepository.findById(id)
                .orElse(null);
    }

    public MunicipalityModel create(@RequestBody Map<String, Object> jsonData) {

        MunicipalityModel municipality = new MunicipalityModel();
        municipality.setMayor(jsonData.get("mayor").toString());
        municipality.setName(jsonData.get("name").toString());

        municipalityRepository.save(municipality);
        return municipality;
    }

    public MunicipalityModel delete(int id) {
        return municipalityRepository.findById(id)
                .map(municipalityModel -> {
                    municipalityRepository.delete(municipalityModel);
                    return municipalityModel;
                })
                .orElse(null);
    }

}
