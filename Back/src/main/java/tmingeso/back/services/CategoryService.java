package tmingeso.back.services;

import tmingeso.back.models.CategoryModel;
import tmingeso.back.repositories.CategoryRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


@Service
@NoArgsConstructor
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<CategoryModel> getAll() {
        return categoryRepository.findAll();
    }

    public CategoryModel getById(int id) {
        return categoryRepository.findById(id)
                .orElse(null);
    }

    public CategoryModel create(@RequestBody Map<String, Object> jsonData) {

        CategoryModel category = new CategoryModel();
        category.setName(jsonData.get("name").toString());

        categoryRepository.save(category);
        return category;
    }

    public CategoryModel delete(int id) {
        return categoryRepository.findById(id)
                .map(categoryModel -> {
                    categoryRepository.delete(categoryModel);
                    return categoryModel;
                })
                .orElse(null);
    }

}
