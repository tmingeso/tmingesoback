package tmingeso.back.services;

import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.models.CategoryModel;
import tmingeso.back.models.UserModel;
import tmingeso.back.repositories.AnnouncementRepository;
import tmingeso.back.repositories.CategoryRepository;
import tmingeso.back.repositories.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@NoArgsConstructor
public class AnnouncementService {

    @Autowired
    private AnnouncementRepository announcementRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;

    String tokenJSon = "token";

    public List<AnnouncementModel> getAll() {
        return announcementRepository.findAll();
    }

    public List<AnnouncementModel> getAllByUserRole(@RequestBody Map<String, Object> jsonData){

        String token = jsonData.get(tokenJSon).toString();
        Logger logger = Logger.getLogger(AnnouncementService.class.getName());
        List<AnnouncementModel> announcementModelList = new ArrayList<>();

        try{
            UserModel user = userRepository.findUserModelByLoginToken(token);

            if (user.getRole().getId() == 3 || user.getRole().getId() == 2){
                return announcementRepository.findAnnouncementModelsByUserNeighborhood(user.getNeighborhood());
            }
            else{
                return announcementRepository.findAnnouncementModelsByUserNeighborhoodMunicipality(user.getNeighborhood().getMunicipality());
            }
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "User Does not exist!!",e);
        }

        return announcementModelList;
    }

    public List<AnnouncementModel> getAllByCategory(@RequestBody Map<String, Object> jsonData){

        String token = jsonData.get(tokenJSon).toString();
        String categoryJson = jsonData.get("category").toString();
        List<AnnouncementModel> announcementModelList = new ArrayList<>();
        Logger logger = Logger.getLogger(AnnouncementService.class.getName());

        try {
            UserModel user = userRepository.findUserModelByLoginToken(token);
            CategoryModel category = categoryRepository.findCategoryModelByName(categoryJson);

            if(user.getRole().getId() == 3 || user.getRole().getId() == 2){
                announcementModelList = announcementRepository.findAnnouncementModelsByUserNeighborhoodAndCategory(user.getNeighborhood(),category);
                return announcementModelList;
            }
            else{
                announcementModelList = announcementRepository.findAnnouncementModelsByUserNeighborhoodMunicipalityAndCategory(user.getNeighborhood().getMunicipality(),category);
                return announcementModelList;
            }
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "Problem with User and Category!!",e);
        }

        return announcementModelList;
    }

    public AnnouncementModel getById(int id) {
        return announcementRepository.findById(id)
                .orElse(null);
    }

    public String create(@RequestBody Map<String, Object> jsonData) {

        AnnouncementModel announcement = new AnnouncementModel();
        Logger logger = Logger.getLogger(AnnouncementService.class.getName());

        try {
            UserModel user = userRepository.findUserModelByLoginToken(jsonData.get(tokenJSon).toString());
            CategoryModel category = categoryRepository.findCategoryModelByName(jsonData.get("category").toString());

            if(Integer.valueOf(jsonData.get("apreciation").toString()) >= 0){
                if(Integer.valueOf(jsonData.get("price").toString()) >= 0){
                    if (user.getRole().getId() == 3){

                        announcement.setUser(user);
                        announcement.setCategory(category);
                        announcement.setTitle(jsonData.get("title").toString());
                        announcement.setDescription(jsonData.get("description").toString());
                        announcement.setDate(jsonData.get("date").toString());
                        announcement.setPrice(Integer.valueOf(jsonData.get("price").toString()));
                        announcement.setApreciation((Integer.valueOf(jsonData.get("apreciation").toString())));
                        announcement.setShown((Integer.valueOf(jsonData.get("shown").toString())));
                        announcement.setContact(user.getPhone());

                        announcementRepository.save(announcement);

                        return "Anuncio publicado con exito!";
                    }
                }
                else{
                    return "Ingrese un precio válido.";
                }
            }
            else{
                return "Ingrese una apreciacion válida.";
            }
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "Problem with User or Category!!",e);
        }
        return "Ha ocurrido un problema ingresando el anuncio";
    }

    public AnnouncementModel delete(int id) {
        return announcementRepository.findById(id)
                .map(announcementModel -> {
                    announcementRepository.delete(announcementModel);
                    return announcementModel;
                })
                .orElse(null);
    }

}
