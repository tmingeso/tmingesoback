package tmingeso.back.services;

import tmingeso.back.models.RoleModel;
import tmingeso.back.repositories.RoleRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


@Service
@NoArgsConstructor
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<RoleModel> getAll() {
        return roleRepository.findAll();
    }

    public RoleModel getById(int id) {
        return roleRepository.findById(id)
                .orElse(null);
    }

    public RoleModel create(@RequestBody Map<String, Object> jsonData) {

        RoleModel role = new RoleModel();
        role.setDescription(jsonData.get("description").toString());
        role.setName(jsonData.get("name").toString());
        roleRepository.save(role);
        return role;
    }

    public RoleModel delete(int id) {
        return roleRepository.findById(id)
                .map(roleModel -> {
                    roleRepository.delete(roleModel);
                    return roleModel;
                })
                .orElse(null);
    }

}
