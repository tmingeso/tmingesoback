package tmingeso.back.services;

import tmingeso.back.models.CommentModel;
import tmingeso.back.models.UserModel;
import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.repositories.CommentRepository;
import tmingeso.back.repositories.UserRepository;
import tmingeso.back.repositories.AnnouncementRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@NoArgsConstructor
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AnnouncementRepository announcementRepository;

    String tokenJSon = "token";

    public List<CommentModel> getAll() {
        return commentRepository.findAll();
    }

    public CommentModel getById(int id) {
        return commentRepository.findById(id)
                .orElse(null);
    }

    public List<CommentModel> getAllByAnnouncementId(@RequestBody Map<String, Object> jsonData){

        List<CommentModel> commentModelList = new ArrayList<>();
        Logger logger = Logger.getLogger(CommentService.class.getName());

        try {
            int announcementId = Integer.parseInt(jsonData.get("announcementId").toString());

            UserModel user = userRepository.findUserModelByLoginToken(jsonData.get(tokenJSon).toString());
            AnnouncementModel announcement = announcementRepository.findAnnouncementModelById(announcementId);

            if (user.getRole().getId() == 3 || user.getRole().getId() == 2) { // Usuario o junta de vecinos
                if (user.getNeighborhood().getId() == announcement.getUser().getNeighborhood().getId()) {
                    commentModelList = commentRepository.findCommentModelsByAnnouncementId(announcementId);
                }
            }
            else { // Municipalidad
                if (user.getNeighborhood().getMunicipality().getId() == announcement.getUser().getNeighborhood().getMunicipality().getId()) {
                    commentModelList = commentRepository.findCommentModelsByAnnouncementId(announcementId);
                }
            }
            return commentModelList;
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "El anuncio correspondiente a la id no fue encontrado.",e);
        }

        return commentModelList;
    }

    public String create(@RequestBody Map<String, Object> jsonData) {

        CommentModel comment = new CommentModel();
        Logger logger = Logger.getLogger(CommentService.class.getName());

        try {
            int announcementId = Integer.parseInt(jsonData.get("announcementId").toString());

            UserModel user = userRepository.findUserModelByLoginToken(jsonData.get(tokenJSon).toString());
            AnnouncementModel announcement = announcementRepository.findAnnouncementModelById(announcementId);

            if (user.getRole().getId() == 3) {
                if (user.getNeighborhood().getId() == announcement.getUser().getNeighborhood().getId()) {
                    comment.setUser(user);
                    comment.setAnnouncement(announcement);
                    comment.setComment(jsonData.get("comment").toString());

                    commentRepository.save(comment);

                    return "Comentario publicado con exito.";
                }
                else {
                    return "El usuario no corresponde a esta junta de vecinos.";
                }
            }
            else {
                return "El usuario no tiene acceso a esta acción.";
            }
        }
        catch (NullPointerException e){
            logger.log(Level.WARNING, "Problema con el ingreso del comentario.",e);
        }
        return "Ha ocurrido un problema ingresando el comentario";
    }

    public CommentModel delete(int id) {
        return commentRepository.findById(id)
                .map(announcementModel -> {
                    commentRepository.delete(announcementModel);
                    return announcementModel;
                })
                .orElse(null);
    }

}
