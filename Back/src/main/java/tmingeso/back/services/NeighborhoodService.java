package tmingeso.back.services;

import tmingeso.back.models.NeighborhoodModel;
import tmingeso.back.repositories.MunicipalityRepository;
import tmingeso.back.repositories.NeighborhoodRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Service
@NoArgsConstructor
public class NeighborhoodService {

    @Autowired
    private NeighborhoodRepository neighborhoodRepository;
    @Autowired
    private MunicipalityRepository municipalityRepository;

    public List<NeighborhoodModel> getAll() {
        return neighborhoodRepository.findAll();
    }

    public NeighborhoodModel getById(int id) {
        return neighborhoodRepository.findById(id)
                .orElse(null);
    }

    public NeighborhoodModel create(@RequestBody Map<String, Object> jsonData) {

        NeighborhoodModel neighborhood = new NeighborhoodModel();

        neighborhood.setNeighborhoodName(jsonData.get("name").toString());
        neighborhood.setNeighborhoodDirection(jsonData.get("direction").toString());
        neighborhood.setNeighborhoodPhone(jsonData.get("phone").toString());
        neighborhood.setNeighborhoodMail(jsonData.get("mail").toString());
        neighborhood.setMunicipality(municipalityRepository.findMunicipalityModelById(Integer.valueOf(jsonData.get("municipality").toString())));
        neighborhoodRepository.save(neighborhood);
        return neighborhood;

    }

    public NeighborhoodModel delete(int id) {
        return neighborhoodRepository.findById(id)
                .map(neighborhoodModel -> {
                    neighborhoodRepository.delete(neighborhoodModel);
                    return neighborhoodModel;
                })
                .orElse(null);
    }
}
