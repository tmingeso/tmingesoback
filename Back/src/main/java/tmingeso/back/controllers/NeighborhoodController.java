package tmingeso.back.controllers;

import tmingeso.back.models.NeighborhoodModel;
import tmingeso.back.services.NeighborhoodService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/neighborhood")
@CrossOrigin(origins = "*")
public class NeighborhoodController {

    private final NeighborhoodService neighborhoodService;

    @GetMapping()
    public List<NeighborhoodModel> getAll(){
        return neighborhoodService.getAll();
    }

    @GetMapping("/{id}")
    public NeighborhoodModel getById(@PathVariable int id) {
        return neighborhoodService.getById(id);
    }

    @PostMapping()
    public NeighborhoodModel create(@RequestBody Map<String, Object> jsonData) {
        return neighborhoodService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public NeighborhoodModel delete(@PathVariable int id) {
        return neighborhoodService.delete(id);
    }

}