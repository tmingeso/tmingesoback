package tmingeso.back.controllers;

import tmingeso.back.models.AnnouncementModel;
import tmingeso.back.services.AnnouncementService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/announcement")
@CrossOrigin(origins = "*")
public class AnnouncementController {

    private final AnnouncementService announcementService;

    @GetMapping()
    public List<AnnouncementModel> getAll(){
        return announcementService.getAll();
    }

    @GetMapping("/byNeighborhood")
    public List<AnnouncementModel> getAllByUserRole(@RequestBody Map<String, Object> jsonData) { return announcementService.getAllByUserRole(jsonData);}

    @PostMapping("/byCategory")
    public List<AnnouncementModel> getAllByCategory(@RequestBody Map<String, Object> jsonData) { return announcementService.getAllByCategory((jsonData)); }

    @GetMapping("/{id}")
    public AnnouncementModel getById(@PathVariable int id) {
        return announcementService.getById(id);
    }

    @PostMapping()
    public String create(@RequestBody Map<String, Object> jsonData) {
        return announcementService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public AnnouncementModel delete(@PathVariable int id) {
        return announcementService.delete(id);
    }

}
