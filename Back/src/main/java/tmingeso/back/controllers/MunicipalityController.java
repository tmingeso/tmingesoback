package tmingeso.back.controllers;

import tmingeso.back.models.MunicipalityModel;
import tmingeso.back.services.MunicipalityService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/municipality")
@CrossOrigin(origins = "*")
public class MunicipalityController {

    private final MunicipalityService municipalityService;

    @GetMapping()
    public List<MunicipalityModel> getAll(){
        return municipalityService.getAll();
    }

    @GetMapping("/{id}")
    public MunicipalityModel getById(@PathVariable int id) {
        return municipalityService.getById(id);
    }

    @PostMapping()
    public MunicipalityModel create(@RequestBody Map<String, Object> jsonData) {
        return municipalityService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public MunicipalityModel delete(@PathVariable int id) {
        return municipalityService.delete(id);
    }

}
