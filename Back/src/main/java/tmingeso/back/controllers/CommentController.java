package tmingeso.back.controllers;

import tmingeso.back.models.CommentModel;
import tmingeso.back.services.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/comment")
@CrossOrigin(origins = "*")
public class CommentController {

    private final CommentService commentService;

    @GetMapping()
    public List<CommentModel> getAll(){
        return commentService.getAll();
    }

    @PostMapping("/byAnnouncement")
    public List<CommentModel> getAllByAnnouncementId(@RequestBody Map<String, Object> jsonData) {
        return commentService.getAllByAnnouncementId(jsonData);
    }

    @GetMapping("/{id}")
    public CommentModel getById(@PathVariable int id) {
        return commentService.getById(id);
    }

    @PostMapping()
    public String create(@RequestBody Map<String, Object> jsonData) {
        return commentService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public CommentModel delete(@PathVariable int id) {
        return commentService.delete(id);
    }

}
