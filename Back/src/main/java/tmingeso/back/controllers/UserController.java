package tmingeso.back.controllers;

import net.minidev.json.JSONObject;
import netscape.javascript.JSObject;
import tmingeso.back.models.UserModel;
import tmingeso.back.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

    private final UserService userService;

    @GetMapping()
    public List<UserModel> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserModel getById(@PathVariable int id) {
        return userService.getById(id);
    }

    @PostMapping("/logIn")
    public String logIn(@RequestBody Map<String, Object> jsonData) { return userService.logIn(jsonData); }

    @PostMapping("/logOut")
    public String logOut(@RequestBody Map<String, Object> jsonData) { return userService.logOut(jsonData); }

    @PostMapping("/ranking")
    public List<JSONObject> userRanking(@RequestBody Map<String, Object> jsonData) { return userService.getUserRanking(jsonData); }

    @PostMapping()
    public UserModel create(@RequestBody Map<String, Object> jsonData) {
        return userService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public UserModel delete(@PathVariable int id) {
        return userService.delete(id);
    }
}
