package tmingeso.back.controllers;

import tmingeso.back.models.CategoryModel;
import tmingeso.back.services.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/category")
@CrossOrigin(origins = "*")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("/all")
    public List<CategoryModel> getAll(){
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public CategoryModel getById(@PathVariable int id) {
        return categoryService.getById(id);
    }

    @PostMapping()
    public CategoryModel create(@RequestBody Map<String, Object> jsonData) {
        return categoryService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public CategoryModel delete(@PathVariable int id) {
        return categoryService.delete(id);
    }

}
