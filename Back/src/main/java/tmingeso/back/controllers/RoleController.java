package tmingeso.back.controllers;

import tmingeso.back.models.RoleModel;
import tmingeso.back.services.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/role")
@CrossOrigin(origins = "*")
public class RoleController {

    private final RoleService roleService;

    @GetMapping()
    public List<RoleModel> getAll(){
        return roleService.getAll();
    }

    @GetMapping("/{id}")
    public RoleModel getById(@PathVariable int id) {
        return roleService.getById(id);
    }

    @PostMapping()
    public RoleModel create(@RequestBody Map<String, Object> jsonData) {
        return roleService.create(jsonData);
    }

    @DeleteMapping("/{id}")
    public RoleModel delete(@PathVariable int id) {
        return roleService.delete(id);
    }

}
