FROM openjdk:11
WORKDIR /project
COPY *.jar app.jar
EXPOSE 8081
CMD java -jar app.jar